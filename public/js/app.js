'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//definiendo funciones basicas
var $ = function $(selector) {
  return document.querySelectorAll(selector) > 1 ? document.querySelectorAll(selector) : document.querySelector(selector);
};

//Definiendo clases

var Task = function () {
  function Task(name) {
    _classCallCheck(this, Task);

    this.name = name;
    this.isComplete = false;
  }

  _createClass(Task, [{
    key: 'complete',
    value: function complete() {
      this.isComplete = !this.isComplete;
    }
  }]);

  return Task;
}();

var TaskList = function () {
  function TaskList(name) {
    _classCallCheck(this, TaskList);

    this.name = name;
    this.tasks = [];
  }

  _createClass(TaskList, [{
    key: 'addTask',
    value: function addTask(task) {
      this.tasks.push(task);
    }
  }, {
    key: 'deleteTask',
    value: function deleteTask(index) {
      this.tasks.splice(index, 1);
    }
  }, {
    key: 'render',
    value: function render(element) {
      var tasks = this.tasks.map(function (task) {
        return '\n    <li class="taskItem ' + (task.isComplete ? 'taskItem--complete' : '') + '">\n      <label class="taskItem__checkContainer">\n        <input class="taskItem__checkbox" type="checkbox" ' + (task.isComplete ? 'checked' : '') + '>\n        <span class="taskItem__completed"></span>\n      </label>\n      <p class="taskItem__title">' + task.name + '</p>\n      <a class="taskItem__delete" href="#">X</a>\n    </li>\n    ';
      });

      element.innerHTML = tasks.reduce(function (a, b) {
        return a + b;
      }, '');
    }
  }]);

  return TaskList;
}();

//Trabajar con el DOM


var addTaskElement = $('.addTask__add');
var taskContainer = $('.listTask');
var inbox = new TaskList('inbox');

// agregar tarea desde el dom

function addDOMTask(e) {
  var list = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : inbox;

  //obtener el texto del input
  if (e.key === 'Enter') {
    //crear la tarea instanciando la clase
    var task = new Task(this.value);
    //agregar la tarea a la lista
    list.addTask(task);
    list.render(taskContainer);

    this.value = '';
  }
}

function getIndex(e) {
  var taskItem = e.target.parentElement,
      tasksItems = [].concat(_toConsumableArray(taskContainer.querySelectorAll('li')));
  return tasksItems.indexOf(taskItem);
}

// eliminar tarea
function removeDOMTask(e) {
  var list = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : inbox;

  //detectar que se hizo click en el enlace
  if (e.target.tagName === 'A') {
    //remover tarea de la lista (se necesita el indice)
    list.deleteTask(getIndex(e));
    list.render(taskContainer);
  }
}

//completar tarea
function completeDOMTask(e) {
  var list = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : inbox;

  //detectar que se hizo click en el span
  if (e.target.tagName === 'SPAN') {
    //completar la tarea de la lista (se necesita el indice)
    var taskItem = e.target.parentElement.parentElement,
        tasksItems = [].concat(_toConsumableArray(taskContainer.querySelectorAll('li'))),
        i = tasksItems.indexOf(taskItem);

    list.tasks[i].complete();

    e.target.parentElement.parentElement.classList.toggle('taskItem--complete');
  }
}

addTaskElement.addEventListener('keyup', addDOMTask);
taskContainer.addEventListener('click', removeDOMTask);
taskContainer.addEventListener('click', completeDOMTask);
//# sourceMappingURL=app.js.map
